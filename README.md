# QGIS Geometry Generator examples

I use **Geometry Generator** a lot in [QGIS](https://qgis.org). I promised to make some of my use cases available online: https://twitter.com/michelstuyts/status/1057049855489646592, so here they are. A great introduction to QGIS Geometry Generator by [Klas Karlsson](https://twitter.com/klaskarlsson) can be found on Youtube: https://www.youtube.com/watch?v=0YxjJ-9zIJ0. This repository contains some of my use cases of Geometry Generators.

All examples in this repository were created in **QGIS 3.x**  with both the project and the layers set to the Lambert 72 Belgium CRS ([EPSG:31370](https://epsg.io/31370)), that has meters as units. In the style settings the units to set sizes "Map Units" and "Meters at scale" are frequently used, so in EPSG:31370 both will result in Meters.

Below you will find an example of all styles currently in this repository.  This repository contains all **QML-files** and **sample data** to try the styles yourself. You can also download this entire repository at once: https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/-/archive/master/qgis-geometry-generator-examples-master.zip

## Dimensions for Polygons
<table><tr><td><a href="QML-files/dimensions/"><img src="Example_images/dimensions_polygon.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/dimensions/)

## Dimensions for Lines
<table><tr><td><a href="QML-files/dimensions/"><img src="Example_images/dimensions_line.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/dimensions/)

## Direction 
### One arrow per line of a polyline
<table><tr><td><a href="QML-files/direction/"><img src="Example_images/direction.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/direction/)

## Labels with a leader 
<table><tr><td><a href="QML-files/label_with_leader/"><img src="Example_images/label_with_leader.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/label_with_leader/)

## Pseudo-random points
<table><tr><td><a href="QML-files/pseudo-random_points_edge/"><img src="Example_images/pseudo-random_points_edge.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/pseudo-random_points_edge/)

## Points as buildings
<table><tr><td><a href="QML-files/points_as_building/"><img src="Example_images/points_as_building.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/points_as_building/)

## Obscurify privacy sensitive data
<table><tr><td><a href="QML-files/obscurify_privacy_sensitive_data/"><img src="Example_images/obscurify_privacy_sensitive_data.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/obscurify_privacy_sensitive_data/)

## Mixed orthogonal stripes at an edge
<table><tr><td><a href="QML-files/mixed_orthogonal_stripes_at_an_edge/"><img src="Example_images/mixed_orthogonal_stripes_at_an_edge.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/mixed_orthogonal_stripes_at_an_edge/)

## Points as tables
<table><tr><td><a href="QML-files/points_as_tables/"><img src="Example_images/points_as_tables.png"></a></td></tr></table> 
[Details of this Geometry Generator Style](QML-files/points_as_tables/)


## Dynamic distance lines
<table><tr><td><a href="QML-files/dynamic_distance_lines/"><img src="Example_images/dynamic_distance_lines.gif"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/dynamic_distance_lines/)

## Holiday Random
<table><tr><td><a href="QML-files/holiday_random/"><img src="Example_images/holiday_random.gif"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/holiday_random/)

## Building Footprint Poster
<table><tr><td><a href="QML-files/building_footprint_poster/"><img src="Example_images/building_footprint_poster.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/building_footprint_poster/)

## Visiualize nearest neighbours
<table><tr><td><a href="QML-files/visiualize_nearest_neighbours/"><img src="Example_images/visiualize_nearest_neighbours.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/visiualize_nearest_neighbours/)

## Numbered nodes
<table><tr><td><a href="QML-files/numbered_nodes/"><img src="Example_images/numbered_nodes.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/numbered_nodes/)